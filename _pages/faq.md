---
layout: single
permalink: "/help/faq"
title: "Frequently Asked Questions"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "help"
---

## General

### Does tinyMediaManager modify files from my library
tinyMediaManager will not alter files from your media library unless you force it to by using:

* scrape meta data (NFO files and artwork files will be created/overwritten)
* rename (the media files including all necessary files like artwork and NFOs will be renamed according to your settings)
* delete (the media files (including all meta data files) of the selected movie(s)/TV show(s)/episode(s) will be deleted from the file system)

### Import watched state into Kodi
By default Kodi does not import the watched state from the NFO into its internal database. You can force Kodi to import the state by following the steps from the [Kodi wiki][2].

### Java Heap Space errors
If you got a large library, the preserved memory (512MB default) for the tinyMediaManager process can be reached. You are able to increase the memory by a dedicated setting (Settings -> General -> System) or by editing a config file directly:

1. open (or create if it does not exist) the file `extra.txt` in the tinyMediaManager folder
2. add (or modify) a new line containing `-Xmx1024m` (where 1024 is the amount of megabytes you want to preserve for tmm; you can change it as you want. **BE AWARE:** this amount of memory has to be free when tinyMediaManager starts)
3. relaunch tinyMediaManager

Java needs this amount of memory to be available (free and in one chunk!) on startup of tinyMediaManager. If there is not enough memory available on your system, Java prevents tinyMediaManager from starting _without_ any message. If you encounter this problem, just try to lower the memory setting.

### Re-import NFO changes in Kodi
Once a movie or TV show has been imported into Kodi, Kodi does not react on (external) changes to the NFO file. You can force Kodi to refresh the data by following the steps on the [Kodi wiki][3].

### How to handle bonus content
Bonus content like cut scenes or some extra clips may cause some import problems when not having the right name scheme (tinyMediaManager detects them as additional movies).

The best way to put bonus content to your movie is to create a subfolder called `extras` inside the movie folder and put all bonus content inside this folder.

Another way would be to add the string `-extras` (or `-behindthescenes`, `-deleted`, `-featurette`, `-interview`, `-scene`, `-short`) at the end of every bonus content file like `Aladdin Cut Scenes-extras.avi`. This way tinyMediaManager knows that these files are extra files for your movie and assigns them to the corresponding movie rather than creating a new movie.

## Installation

### How big is tinyMediaManager
tinyMediaManager itself needs (depending on the operating system you use) about 30 - 40MB. Since tinyMediaManager stores all data (internal database, logs, cache, ..) inside its own folder the final disk usage of tinyMediaManager heavily depends on size of your media library. This can be easily  some hundreds of megabytes up to several gigabytes.

### Where should I install tinyMediaManager
**Windows**

tinyMediaManager is designed to be a portable application: this means that you can simply extract it to your preferred location (e.g. the users directory, or any other hard drive/network share). There is no need to install tinyMediaManager into C:\Program Files\.

**macOS**

After downloading the archive, simply extract it and move the tinyMediaManager app to your Applications directory. Executing from the downloads folder is not possible due to security mechanics of macOS.

**Linux**

We'd suggest you to extract tinyMediaManager to your home directory. tinyMediaManager stores all data inside its own directory, this is why the users directory would fit best.

## Errors and bugs

### Scraping does sometimes not work
Sometimes we receive garbled/unparseable responses from the meta data providers (or some devices along the connection to the servers like gateways). Since we are using some sort of HTTP caching, these unparseable responses are cached for a while with a result, that a further scraping will also result in an error.

If you encounter that problem you can try to clear the cache via the menu

* Tools -> Clear HTTP cache

### tinyMediaManager won't start
We've built tinyMediaManager as fail safe as possible. But there are always combinations of Java/OS/configurations possible which could prevent it from starting. Have a look at the OS specific sections or at the *Java Heap Space* sections if there are any hints for getting tinyMediaManager to start.

If the hints did not help, please collect the following logs which are produced by two steps of starting tinyMediaManager:

* The updater, which fetches the latest version of tinyMediaManager directly from the web (launcher.log)
* Launching of tinyMediaManager itself (logs/tmm.log)

In the case tinyMediaManager won't bring up his own UI, please create a bug report at [GitLab][10] and attach the log files there.

### Blank window on startup
The [Installation Page][1] has more info and a possible fix.

### UI glitches with a remote desktop connection
If you use tinyMediaManager with a remote desktop connection software like RDP or VNC, Java can produce some weird glitches due a lack of 3D rendering with remote desktop connections.

In this case, try to create (or edit if it already exists) a file called extra.txt in the tinyMediaManager install directory and add the following line
```
-Dsun.java2d.d3d=false
```
and restart tinyMediaManager

## Windows

### tinyMediaManager is unreadable with a high DPI monitor
Java for Windows does handle high DPI display different to other systems and you may need to do different things depending on your installed Java version:

Java 8: follow the steps from [this thread][8]

Java 9+: either do the same as for Java 8 or create a file called `extra.txt` in the tinyMediaManager install directory and add the following line

```
-Dsun.java2d.uiScale=2
```

last but not least you could try to increase the font size in the general settings and restart tinyMediaManager

### tinyMediaManager can't connect to the internet viy NTLM proxy
NTLM authentification does not work within tinyMediaManager. You can use something like [CNTLM][6] to connect to the internet.

### The user interface of tinyMediaManager is corrupted
Try to create (or edit if it already exists) a file called `extra.txt` in the tinyMediaManager install directory and add the following line

```
-Dsun.java2d.nodraw=true
```

and restart tinyMediaManager

### tinyMediaManager won't start with Java Portable
Java Portable provides Java in a different way than the installed one. You may need to create a batch file (.bat) inside the tinyMediaManager folder with the following content (including the dot at the end!):
```
cd %~dp0
<path to PortableApps>\CommonFiles\Java\bin\javaw.exe -Djava.net.preferIPv4Stack=true -Dsilent=noupdate -Dfile.encoding=UTF-8 -jar getdown.jar .
```

replace `<path to PortableApps>` with the installation path of your PortableApps.

## Mac OSX

### tinyMediaManager won't start

* make sure you copied tinyMediaManager to your applications folder after downloading. For security reasons OSX executes apps from the downloads folder in a read only sandbox but tinyMediaManager needs write access (to store settings, database, logs, ...)
* make sure Java is installed (open the terminal/command line via Spotlight -> terminal) and type
  `java -version` this should print the installed Java version of your system (1.6, 1.7 or 1.8). If the program is not found, please install the latest Java version
* if Java is installed, but tinyMediaManager won't start, there could be a Java library in your /Library/Java/Extensions which interfere with tinyMediaManager. In this case try to create a file called `extra.txt` in your tinyMediaManager folder (right click the tinyMediaManager.app -> Show package contents -> navigate to Contents - Resources - Java) with the following content: `-Djava.ext.dirs=`
* if tinyMediaManager still won't start, please have a look at the FAQ for reporting bugs

## Linux

### tinyMediaManager is unreadable with a high DPI monitor
For having high DPI support on Linux you need to have at least Java 9+ installed. If installing Java 9+ itself does not solve the issue, please follow the [Arch Linux Wiki][9].

### tinyMediaManager won't start
Some Linux distributions only provide a headless version of Java per default (this is the core part of Java without any UI libraries). Make sure you also have the UI part of Java installed. Find more details in the [Installation Page][11].

### libmediainfo does not load
libmediainfo is a native library which has to be compiled for every distribution/release/arch. We ship a pre packaged version of libmediainfo along with tinyMediaManager which should be suitable for most Linux users. If that version does not work for you, please try to install libmediainfo from your distribution (that is being loaded as fallback when the shipped one does not load). Find more details in the [Installation Page][11].

### Problems importing movies with special characters in their name
If you have problems to import movies with a special character in their name, check if the locale en_US.UTF-8 has been generated on your system.
If not, have a look at the documentation of your linux distribution how to generate them. On Arch Linux/Manjaro the steps would be:

* login as root
* uncomment the line `en_US.UTF-8 UTF-8` in the file `/etc/locale.gen`
* run `locale-gen` as root
* logout and login again

### Ugly font rendering in Linux
Due to a bug in the JVM, fonts are drawn rather ugly in Linux desktop environments. We did as much improvements (with rendering parameters) as possible. If using a newer JVM (v1.8) is not possible for you, there is the option to install a "patched JVM". There are packages for [Ubuntu][4] or [Arch Linux][5]. You may find similar packages for other Linux distributions as well.

[1]: /docs/installation#blank-window
[2]: http://kodi.wiki/view/Import-export_library#Watched_status
[3]: http://kodi.wiki/view/Set_content_and_scan#Refreshing_the_library
[4]: http://www.webupd8.org/2013/06/install-openjdk-patched-with-font-fixes.html
[5]: https://aur.archlinux.org/packages/jre7-openjdk-infinality/
[6]: http://cntlm.sourceforge.net/
[7]: http://java.com/de/download/
[8]: https://superuser.com/questions/988379/how-do-i-run-java-apps-upscaled-on-a-high-dpi-display/1207925#1207925
[9]: https://wiki.archlinux.org/index.php/HiDPI#Java_applications
[10]: https://gitlab.com/tinyMediaManager/tinyMediaManager/issues
[11]: /docs/installation
