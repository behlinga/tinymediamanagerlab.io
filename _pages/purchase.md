---
title: "Purchase a tinyMediaManager license"

layout: single
permalink: "/purchase/"

sidebar:
  nav: "purchase"

paddle: true
---
<script type="text/javascript">
	Paddle.Setup({ vendor: 111552 });
</script>

<h1>Purchase a license</h1>

You can [download](/download) and evaluate tinyMediaManager for free (with some limitations).

If you want to unlock all features of tinyMediaManager you need to buy a 1-year license via [Paddle](https://www.paddle.com). The license is per user, so you can install tinyMediaManager on as many machines as you wish.

<div id="purchase">
  <a id="paddle" href="#" class="paddle_button" data-allow-quantity="false" data-referrer="purchase" data-product="589766" data-init="true">Purchase <em>1 year</em> license</a>
</div>
<br />

Please take care of the following things when buying a license:

- The whole payment system is done via [Paddle](https://www.paddle.com). If you encounter any problems while purchasing a license clease [contact Paddle](https://paddle.net/charge) for further help.
- Please type your email address correctly when purchasing a license. The license code will be sent to this email address after the purchase is finished.
- If you did not received an email from [Paddle](https://www.paddle.com) please check your *Spam / Junk Email folder*. If you have not received any email at all then you have likely mistyped your email address or your mail server is rejecting emails. In this case, please contact [contact Paddle](https://paddle.net/charge) with *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.
- A license is valid on all platforms and can be used on multiple machines.
- After you have received your license code, you can unlock tinyMediaManager directly from within the application. If you use the command line version, just paste the license code **without any extra spaces/newlines** into a file called `/data/tmm.lic` inside your tinyMediaManager folder.
- If you have lost your license code, please [contact Paddle](https://paddle.net/charge) with your *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.
