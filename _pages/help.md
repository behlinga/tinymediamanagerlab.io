---
layout: single
classes: wide
permalink: "/help/"
title: "Help"

sidebar:
  nav: "help"
---

You can find help at

* our [documentation](/docs/)
* the [Frequently Asked Questions](/help/faq)
* the tinyMediaManger section in the [Kodi Forums](https://forum.kodi.tv/forumdisplay.php?fid=204)
* on our [subreddit](https://www.reddit.com/r/tinyMediaManager/)

or you can submit a [Bug Report](https://gitlab.com/tinyMediaManager/tinyMediaManager/issues/new?issuable_template=Bug) or [Feature Request](https://gitlab.com/tinyMediaManager/tinyMediaManager/issues/new?issuable_template=Feature%20Request).
