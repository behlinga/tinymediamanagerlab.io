---
title: "Download"
layout: single
classes: wide

permalink: /download/release-v2/

sidebar:
  nav: "downloads"
---

You need at least Java 1.7 to run tinyMediaManager v2. You can get Java from [https://www.java.com/](https://www.java.com/) (Linux users can download Java from the package repository of their distribution too).

<object type="text/html" data="https://release.tinymediamanager.org/download_v2.html" style="width:100%; height:400px;">
<p>You find the latest release at <a href="https://release.tinymediamanager.org">https://release.tinymediamanager.org</a></p>
</object>
