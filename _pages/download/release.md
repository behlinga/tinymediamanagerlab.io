---
title: "Download"
layout: single
classes: wide

permalink: /download/

sidebar:
  nav: "downloads"
---

You need at least Java 1.8 to run tinyMediaManager v3. You can get Java from [https://www.java.com/](https://www.java.com/) (Linux users can download Java from the package repository of their distribution too).

<object type="text/html" data="https://release.tinymediamanager.org/download_v3.html" style="width:100%; height:400px;">
<p>You find the latest release at <a href="https://release.tinymediamanager.org">https://release.tinymediamanager.org</a></p>
</object>
