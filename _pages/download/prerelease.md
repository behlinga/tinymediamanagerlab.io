---
title: Pre-Release download
layout: single
classes: wide

permalink: /download/prerelease/

sidebar:
  nav: "downloads"
---

Pre-Release builds are built a few days before we publish a new release. This build is rather stable but still can contain some bugs. This build should be used for testing purposes only.

You need at least Java 1.8 to run tinyMediaManager v3. You can get Java from [https://www.java.com/](https://www.java.com/) (Linux users can download Java from the package repository of their distribution too).

<object type="text/html" data="https://prerelease.tinymediamanager.org/download_v3.html" style="width:100%; height:400px;">
<p>You find the latest builds at <a href="https://prerelease.tinymediamanager.org">https://prerelease.tinymediamanager.org</a></p>
</object>
