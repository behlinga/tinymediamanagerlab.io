---
title: Nightly Build download
layout: single
classes: wide

permalink: /download/nightly-build/

sidebar:
  nav: "downloads"
---

Nightly builds are automatically built every day and should only be used for hunting down bugs on **test files**. These build may contain severe bugs which can delete/destroy your test files!

You need at least Java 1.8 to run tinyMediaManager v3. You can get Java from [https://www.java.com/](https://www.java.com/) (Linux users can download Java from the package repository of their distribution too).

<object type="text/html" data="https://nightly.tinymediamanager.org/download_v3.html" style="width:100%; height:400px; ">
<p>You find the latest nightly builds at <a href="https://nightly.tinymediamanager.org">https://nightly.tinymediamanager.org</a></p>
</object>
