---
permalink: /imprint/
title: "Imprint"
author_profile: true
---

# Imprint
Information in accordance with §5 of the E-Commerce Act, §14 of the Unternehmensgesetzbuch, §63 of the Commercial Code and disclosure requirements under §25 of the Media Act.

**Manuel Laggner**  
**Koglehen 2,**  
**6391 Fieberbrunn,**  
**Austria**

Phone: **+43 681 20329289**  
Email: **office@tinymediamanager.org**

Object of the company: **IT Services**  
VAT-Number: **ATU75739916**  

[![ECG](/images/ecg.webp)](https://firmen.wko.at/manuel-ronald-laggner%2c-msc/tirol/?firmaid=70ec5db0-2f72-49aa-9ac2-bcc626ecfb7d)

## EU Dispute Resolution
According to the Ordinance on Online Dispute Resolution in Consumer Matters (ODR Ordinance) we would like to inform you about the Online Dispute Resolution Platform (OS Platform).
Consumers have the possibility to submit complaints to the Online Dispute Resolution Platform of the European Commission at [http://ec.europa.eu/odr?tid=121406481](http://ec.europa.eu/odr?tid=121406481). You will find the necessary contact details above in our imprint.

However, we would like to point out that we are not willing or obliged to participate in dispute resolution proceedings before a consumer arbitration board.

## Liability for contents of this website
We are constantly developing the contents of this website and make every effort to provide correct and up-to-date information. Unfortunately, we cannot assume any liability for the accuracy of any content on this website, especially for those provided by third parties.

Should you notice any problematic or illegal content, please contact us immediately, you will find the contact details in the imprint.

## Liability for links on this website
Our website contains links to other websites for whose content we are not responsible. According to § 17 ECG, we are not liable for linked websites, because we had and have no knowledge of illegal activities, we have not noticed any such illegality and we would remove links immediately if we became aware of any illegality.

If you notice illegal links on our website, please contact us, you will find the contact details in the imprint.

## Copyright notice
All contents of this website (pictures, photos, texts, videos) are subject to copyright. If necessary, we will legally pursue the unauthorized use of parts of the contents of our site.

If you find any content on this website that violates copyright law, please contact us.

Source: Created with the Impressum Generator by [AdSimple® Blog Marketing](https://www.adsimple.at/blog-marketing/) in cooperation with [bauguide.at](https://www.bauguide.at/)