---
layout: single
permalink: "/docs/register"
title: "Register license code"

sidebar:
  nav: "docs"
---
# Register the license code

When purchasing a tinyMediaManager license, you will get an email from [Paddle](https://www.paddle.com) with your license code. To unlock all features in tinyMediaManager, you can add the license code to tinyMediaManager in two different ways:

1. You can enter the license code either directly in tinyMediaManager (**Unlock** button on the right side of the toolbar or via the menu **Info -> Register tinyMediaManager**)
2. If you use the command line version, just paste the license code without any extra spaces/newlines into a file called `/data/tmm.lic` inside your tinyMediaManager folder.

### Can I use the license on more than one tinyMediaManager instances?
The license is per user - you can use it on as many tinyMediaManager installations as you wish.

### Where is the license information stored?
tinyMediaManager stores the license code in a file called `/data/tmm.lic` inside the tinyMediaManager folder.

### I did not receive any license
If you did not received an email from [Paddle](https://www.paddle.com) please check your *Spam / Junk Email folder* (if you use a desktop application, please check that folders on the web interface too). If you have not received any email at all then you have likely mistyped your email address or your mail server is rejecting emails. In this case, please contact [contact Paddle](https://paddle.net/charge) with *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.

### I have lost my license code
If you have lost your license code, please [contact Paddle](https://paddle.net/charge) with your *name*, *email* and *payment details* so they can look up your order and resend your license confirmation.
