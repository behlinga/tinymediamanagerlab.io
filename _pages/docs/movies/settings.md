---
layout: single
permalink: "/docs/movies/settings"
title: "Movie Settings"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

# Movie Settings #

## Movies ##

### UI Settings ###

* **Persist UI filters**: when active, the filter set in the movie list will be stored and loaded on tinyMediaManager start up.
* **Show media logos**: Show/hide the media logos panel in the movie details view
* **Preferred rating**: tinyMediaManager supportes multiple ratings for every movie. The rating source in this setting indicates which rating source should be preferred for being used in the UI.
* **Personal rating first**: when active any personal rating will be preferred over all other ratings

### Automatic Tasks ###

* **Automatic rename**: you can let tinyMediaManager automatically rename your movies after they have been scraped
* **Automatic sync with Trakt.tv**: if you have enabled access to Trakt.tv, you are able to automatically sync your media library and watched state with Trakt.tv. Is that option is disabled, you have to manually trigger the synchronization process

### Misc. Settings ###

* **Build image cache on import**: when importing new movies into tinyMediaManager you can create the image cache on the fly (only available when the image cache is enabled in the general settings). **Caution**: building the image cache will take a while
* **Prefer runtime from media info**: when activating this setting, the movie runtime is being taken directly from your movie file rather than the scraper
* **Include external audio streams in NFO**: using this setting tinyMediaManager will add external audio streams to the `streamdetails` section in the NFO files
* **The following images types will be checked for artwork completeness**: tinyMediaManager needs to know which artwork types must be available to know if the movie artwork is complete. This is needed for the artwork column in the movie list and the action _Download missing artwork_.

### Media Center Presets ###

tinyMediaManager has presets for various media centers. By pressing the button for your desired media center, all affected settings will be set to the default for the chosen media center.

Currently we support presets for:

* Kodi (17+)
* XBMC/Kodi (<17)
* MediaPortal 1.x (Moving Pictures and MyVideo)
* MediaPortal 2.x
* Plex

## Data Sources ##

### Data Sources ###

This is the core setting of the movie section. Every folder specified in this list will be searched for movie files. Only movies file *in and beneath* this folder will be found by the _update data sources_ action. Every movie _should_ be in its own folder, but **having multiple movies per folder is also supported, but will deactivate some features!** tinyMediaManager (and Java) supports accessing local drives and network shares, but is unable to perform mount/logon actions. You have to connect to external drives from your system **before** starting an _update data sources_ in tinyMediaManager!

To achieve the best possible experience, make sure you have a similar setup (e.g. H:\movies is one data source set in the settings):

```
H:\movies
    Aladdin
        Aladdin.avi
        movie.nfo
        sample
            sample.avi
    Alice in Wonderland
        Alice.in.Wonderland.mkv
        movie.nfo
        fanart.jpg
        poster.jpg
    Cars
        Cars.avi
    ...
```

### Exclude Folder(s) from Scan ##

Every folder in this list will be excluded from a scan. You can also put a file called `.tmmignore` into every folder you wish to have excluded from the scan.

### Bad Words ###

Words from this list will be removed from file names while parsing for the title. This comes in handy if you some special words like release groups in your file names.

## Scraper ##

In this list you can choose your preferred metadata scraper from a list of all available scrapers. At the moment tinyMediaManager offers scrapers for the following meta data sources:

* themoviedb.org
* imdb.com
* moviemeter.nl
* mpdb.tv
* trakt.tv
* omdbapi.com
* ofdb.de

And additionally there are two meta scrapers which offer enhanced data:

* **Universal scraper**: with this scraper you can combine results from various other scrapers to create an individual scraping result for your needs
* **Kodi scraper**: this meta scraper is able to parse scrapers from Kodi to embed them into tinyMediaManager. This scraper searches for local installed Kodi instances to use the scrapers from Kodi.

You will find a detailed description of those scrapers on the [scraper description page](/docs/scrapers).

## Scraper Options ##

### Advanced Options ###

* **Preferred language**: Choose the preferred language for scraping (for localized content like title, tagline and plot). Not all scrapers offer localized content, but tinyMediaManager tries to find localized in this language.
* **Certification country**: Movie certifications are available for several countries. You can choose for which country tinyMediaManager should try to get the certification.
* **Fall back to other scrapers**: If nothing has been found with your preferred scraper, try to search with other scrapers
* **Capitalize first letter of every word in title and original title**: As the option itself tells - when activating this, tinyMediaManager puts the found title/original title to *Title Case*.

### Metadata Scrape Defaults ###

In this section you can set which types of metadata should be scraped per default. You can always override this in the scrape dialogs in tinyMediaManager.

The command line version of tinyMediaManager completely relies on this setting.

### Images ###

* **Automatically scrape images**: with this option enabled, tinyMediaManager tries to find the best image files from the artwork scrapers according to your settings in the _Images_ section.

### Automatic Scraper ###

When doing an automatic scrape, tinyMediaManager calculates a score of every found movie. 1.0 means the search term and the search result are 100% the same whereas 0.0 means they are absolutely different. With this setting you can set the threshold starting of which score tinyMediaManager should take the found movie.

## NFO Settings ##

* **NFO format**: There is support for Kodi and MediaPortal NFO formats. Choosing the right format affects how the data is written to the NFO files.
* **NFO file naming**: You can choose between different file names for the NFO files. If no NFO file name is selected, tinyMediaManager does not write any NFO files.
  * \<movie_filename\>.nfo
  * movie.nfo (only works within "one movie per folder" scheme)
* **Write clean NFO**: If this option is activated, tinyMediaManager write a _clean_ NFO file without embedding _unknown/unsupported_ data from existing NFO files.
* **NFO language**: The language in which texts like genre names should be written to the NFO file
* **Certification format**: The format of the data in the \<certification\> tag.

## Images ##

### Artwork Scraper ###

In this list you can choose the artwork scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible artwork. At the moment tinyMediaManager offers scrapers for the following artwork sources:

* themoviedb.org
* fanart.tv
* kyradb.com

### Advanced Options ###

* **Preferred language**: Get the artwork with text in the given language. `-` indicates that tinyMediaManager should look for artwork without any texts on it. Please note that not all artwork sources offer the needed information for this to work (it is known that themoviedb.org and fanart.tv work with this setting).
* **Poster size**: Choose the preferred poster size. tinyMediaManager will try to get the poster in this size (and the chosen language). If it’s not available an image from the other sizes will be taken.
* **Fanart size**: Choose the preferred fanart size. tinyMediaManager will try to get the fanart in this size (and the chosen language). If it’s not available an image from the other sizes will be taken.
* **Prefer language over resolution when choosing images**: If the desired artwork size has not been found in the desired language, take a lower size image with the preferred language rather than a bigger artwork in another language

## Artwork Filenames ##

Generally speaking artwork filenames either take the movie filename as a base an postfixes it with their type or use some filename independent naming scheme. Here you will find an example how this works:

| Option | Description | Example filename (Aladdin.mkv) |
|--------|-------------|-----------------------|
|\<movie filename\>.ext | movie filename + extension of the original image file | Aladdin.jpg |
|\<movie filename\>-poster.ext | movie filename + -poster + extension of the original image file | Aladdin-poster.jpg  |
|poster.ext | poster + extension of the original image file | poster.jpg |
|folder.ext | folder + extension of the original image file | folder.jpg |
|movie.ext | movie + extension of the original image file | movie.jpg |

`.ext` stands for the image format of the artwork file which should be either `.png`, `.jpg` or `.gif`.

**BE AWARE**: All movie filename independent artwork option only work for single movie folders! Whenever tinyMediaManager detects that your movie is in a multi movie folder, it will fall back to `<movie filename>-<type>.ext` (Aladdin-poster.jpg for example).

The following artwork types and filenames are available:

* **Poster**
  * \<movie filename\>-poster.ext
  * \<movie filename\>.ext
  * poster.ext
  * movie.ext
  * folder.ext
* **Fanart**
  * \<movie filename\>-fanart.ext
  * \<movie filename\>.fanart.ext
  * fanart.ext
* **Banner**
  * \<movie filename\>-banner.ext  
  * banner.ext
* **Clearart**
  * \<movie filename\>-clearart.ext  
  * clearart.ext
* **Thumb**
  * \<movie filename\>-thumb.ext  
  * thumb.ext
  * \<movie filename\>-landscape.ext  
  * landscape.ext
* **Logo**
  * \<movie filename\>-logo.ext  
  * logo.ext
* **Clearlogo**
  * \<movie filename\>-clearlogo.ext    
  * clearlogo.ext
* **Disc art**
  * \<movie filename\>-disc.ext    
  * disc.ext
  * \<movie filename\>-discart.ext  
  * discart.ext
* **Keyart**
  * \<movie filename\>-keyart.ext  
  * keyart.ext

## Enable Extra Artwork ##

* **Enable extrathumbs**: This option allows you to store multiple thumbs to the `extrathumbs` folder inside the movie folder. Only works for movies in dedicated folders.
 * **Resize extrathumbs**: With this option you are able to scale down the extrathumbs to a given width.
 * **Maximum of downloaded images**: In addition to choose the extrathumbs by hand (image chooser dialog) you can automatically download extrathumbs. This option let you choose how much extrathumbs will be downloaded per movie.
* **Enable extrafanarts**: This option allows you to store multiple fanarts to the extrafanarts folder inside the movie folder. Only works for movies in dedicated folders
 * **Maximum of downloaded images**: in addition to choose the extrafanarts by hand (image chooser dialog) you can automatically download extrafanarts. This option let you choose how much extrafanarts will be downloaded per movie.
* **Download actor images to .actors**: Kodi supports reading of actor images from the (hidden) folder `.actors` inside the movie folder. If you enable this option, all available actor images will be downloaded to this folder upon scraping the movie.
* **Store movie set artwork in each movie folder**: Allow tinyMediaManager to store a copy of the movie set artwork in each assigned movie folder.
* **Store movie set artwork in a separate folder**: In addition with the Kodi plugin `Movie Set Artwork Automator` tinyMediaManager allows to store the artwork for movie sets in this folder. Here you have to choose an direct path to the folder (it is not set per data source!). E.g: H:\MoviesetArtwork

## Trailer ##

In this list you can enable all wanted trailer scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible trailers. At the moment tinyMediaManager offers scrapers for the following trailer sources:

* themoviedb.org
* hd-trailers.net
* ofdb.de

### Advanced Options ###

* **Use preferred trailer settings**: Rather than choosing the first available trailer, you can set your preferred trailer source (e.g. Youtube) and trailer quality (e.g. 1080p).
* **Automatic trailer download**: Upon scraping, also download the chosen trailer to your movie folder if no local trailer has been found
* **Trailer file naming**: You can set the desired trailer filename. If no desired filename is set, no trailer will be downloaded.
  * \<movie filename\>-trailer.ext
  * movie-trailer.ext

## Subtitles ##

In this list you can enable all wanted subtitle scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible subtitles. At the moment tinyMediaManager offers scrapers for the following subtitle sources:

* opensubtitles.org

### Advanced Options ###

* **Preferred language**: The preferred language for subtitle download.
* **Subtitle language style**: Save subtitle files with the given language style in their filename.

## Renamer ##

tinyMediaManager offers a powerful renamer to rename your movies and all associated files to your desired folder-/filenames. While there is almost nothing you can't do with the renamer, it has still one big restriction: you can only rename the movies inside its own data source. Renaming it to a destination which is not inside the own data source is not supported.

### Renamer Pattern ###

* **Folder name** and **Filename**: Choose the desired folder name and filename for renaming. Here you can enter fixed parts of the name and dynamic ones. You will find a list of all available tokens for building up dynamic names beneath the settings along with examples of your media library. With leaving the folder name/filename empty, the renamer will skip the name generation for the empty part.

### Advanced Options ###

* **Replace spaces in folder name** and **Replace spaces in filename**: You can replace all spaces with either underscores, dots or dashes by activation this option.
* **Replace colons with**: Since colons are illegal characters in folder names and filenames, tinyMediaManger offers you to choose how they should be replaced.
* **Replace non ASCII characters with their basic ASCII equivalents**: Some file systems might need to have ASCII conform file names - by activating this option tinyMediaManager tries to convert non ASCII characters into a similar ASCII character (e.g. Ä - Ae; ß - ss, ...)
* **Enable movie set tokens also for movie sets containing only one movie**: Unless this is activated, the renamer only creates a movie set folder if there is more than one movie in the set.
* **Remove all other NFOs**: Remove all non tinyMediaManager NFOs when renaming (clean up)   

The renamer is built on [JMTE](https://gitlab.com/tinyMediaManager/tinyMediaManager/wikis/JMTE) and can profit from all options you can have in JMTE. For easier usage, we have added some preset tokens (see table below). Nevertheless you can also access every field from the movie just by using the full syntax like `${movie.title}`.

Available tokens:

| Token | Description | Example |
|-------|-------------|---------|
|${title}<br>${movie.title} |	Title|	One Hundred and One Dalmatians|
|${originalTitle}<br>${movie.originalTitle}	|Original title|	One Hundred and One Dalmatians|
|${title[0]}<br>${movie.title[0]}	|First character of the title|	O|
|${title;first}<br>${movie.title;first}	|First letter or # (for non letters)|	O|
|${title[0,2]}<br>${movie.title[0,2]}	|First two characters of the title|	On|
|${titleSortable}<br>${movie.titleSortable}	|Title sorting (e.g. Matrix, The)|	One Hundred and One Dalmatians|
|${year}<br>${movie.year}	|Year|	1961|
|${movieSet.title}	|Movieset title|	101 Dalmatians (Animated) Collection|
|${movieSet.titleSortable}	|Movieset title (sort title)|	101 Dalmatians (Animated) Collection|
|${rating}<br>${movie.rating.rating}	|Rating|	6.7|
|${imdb}<br>${movie.imdbId}	|IMDb number|	tt0055254|
|${certification}<br>${movie.certification}	|Certification|	G|
|${directors[0].name}<br>${movie.directors[0].name}	|Name of first director|	Clyde Geronimi|
|${genres[0]}<br>${movie.genres[0]}	|First genre (localized name if available)|	Adventure|
|${genres[0].name}<br>${movie.genres[0].name}	|First genre (English)|	Adventure|
|${genresAsString}<br>${movie.genresAsString}	|All genres separated by ', '|	Adventure, Animation, Comedy, Family|
|${tags[0]}<br>${movie.tags[0]}	|First tag|
|${language}<br>${movie.spokenLanguages}	|Language|	en|
|${videoResolution}<br>${movie.mediaInfoVideoResolution}	|Video resolution (e.g. 1280x720)|1920x1080|
|${videoCodec}<br>${movie.mediaInfoVideoCodec}	|Video codec (e.g. H.264)|h264|
|${videoFormat}<br>${movie.mediaInfoVideoFormat}	|Video format (e.g. 720p)|1080p|
|${videoBitDepth}<br>${movie.mediaInfoVideoBitDepth}	|Video bit depth (e.g. 8 / 10)|8|
|${audioCodec}<br>${movie.mediaInfoAudioCodec}	|Audio codec of the default/first stream (e.g. AC3)|AC3|
|${audioCodecList}<br>${movie.mediaInfoAudioCodecList}	|Array of all audio codecs (e.g. [AC3, MP3]) |	[AC3, MP3]|
|${audioCodecsAsString}<br>${movie.mediaInfoAudioCodecList;array}	|List of all audio codecs (e.g. AC3, MP3)|	AC3, MP3 |
|${audioChannels}<br>${movie.mediaInfoAudioChannels}	|Audio channels of the default/first stream (e.g. 6ch)|	6ch|
|${audioChannelList}<br>${movie.mediaInfoAudioChannelList}	|Array of all audio channels (e.g. [6ch, 2ch])|	[6ch, 2ch] |
|${audioChannelsAsString}<br>${movie.mediaInfoAudioChannelList;array}	|List of all audio channels (e.g. 6ch, 2ch)|	6ch, 2ch|
|${audioLanguage}<br>${movie.mediaInfoAudioLanguage}	|Audio language of the default/first steam (e.g. EN)|	EN
|${audioLanguageList}<br>${movie.mediaInfoAudioLanguageList}	|Array of all audio languages (e.g. [EN, DE])|	[EN, DE]|
|${audioLanguagesAsString}<br>${movie.mediaInfoAudioLanguageList;array}	|List of all audio languages (e.g. EN, DE)| EN, DE|
|${mediaSource}<br>${movie.mediaSource}	|Media source (DVD etc.)|BluRay|
|${3Dformat}<br>${movie.video3DFormat}	|3D tag (3D SBS/TAB) |3D|
|${hdr}<br>${movie.videoHDRFormat}	|High Dynamic Range (HDR) |HDR|
|${edition}<br>${movie.edition}	|Edition	|Directors Cut|
|${filesize}<br>${movie.videoFilesize;filesize} | File size in GB | 1.05 G |
|${note}<br>${movie.note} | Movie note | My personal note |

Some hints for the renamer:

* If there wouldn't be a change (old and new file-/folder name are identical), the renamer skips renaming
* All supported files within the movie folder (NFO, poster, fanart, …) will be renamed as configured (in the last section)
* you can also use some pre-built renderers to modify the result; you cannot use the preset token for the renderers to work - you need to use the full syntax:
  * _UPPER_ case renderer `upper`: `${movie.title;upper}`
  * _lower_ case renderer `lower`: `${movie.title;lower}`
  * _Title_ case renderer `title`: `${movie.title;title}`
  * Date format renderer `date`: `${movie.releaseDate;date(yyyy-MM-dd)}` would cause the release date to be formatted with a renderer named "date". This named renderer would then be passed the format "yyyy-MM-dd" along with the variable to be rendered.
  * File name renderer `filename`: `${movie;filename(escape=true)}` would a valid file name for the movie (according to your renamer settings) - useful for linking between master and detail pages. The parameter `escape=true` can be used to create an escaped url

### Example ###

Here you can see the renamer results using an example from your library.
