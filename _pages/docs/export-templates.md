---
layout: single
permalink: "/docs/exporttemplates"
title: "Export Templates"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

# Export Templates

tinyMediaManager uses the Java Minimal Template Engine ([JMTE][1]) to construct the exported page. You will find the JMTE Language reference in the [docs][5].

## Creating templates
If you want to edit or create a new template make a copy of the template folder you would like to build upon. The template folders are in the tinyMediaManager folder (or inside the tinyMediaManager.app -> `/Contents/Resources/Java/templates` on macOS). **Do NOT edit the default templates as they will be overwritten each time you start tinyMediaManger and you will lose your changes!**

If you create a nice template and want us to ship it to all users, contact us at [GitLab][4], the [Kodi forums][2] or [reddit][3] and we will review it for distribution with the program.  
You can also send plain HTML templates; we would integrate the functionality for you ;)

## Template setup
Templates rely on three files to export successfully. All other files you create will also be exported, retaining their directory structure, when the page is built by tMM; this allows for the inclusion of style sheets, images and scripts.
* **template.conf** - This configuration file tells tMM where to find the other required files.
* **list.jmte** - Template for creating a list of your movies/shows (maybe with links to detail pages)
* **detail.jmte** - Template for detailed information of one movie or show. detail.jmte is required only if you want tMM to build individual <movie>.xxx files for inclusion into index.html either through an .ajax() call or iframe.
* **episode.jmte** - episode.jmte is required only if you want tMM to build individual <episode>.xxx files for inclusion into index.html/detail.html either through an .ajax() call or iframe.

Each template must be in its own directory and include a template.conf file. The contents of template.conf must include:
* **name=\<name of template\>** - The name that will display to the user when exporting through the UI.
* **type={movie, tv_show}** - Currently only movie/tv show templates are supported.
* **list=\<path to list.jmte\>** - (default: list.jmte) This is the template which will be used to build index.html or movielist.xml/csv.
* **detail=\<path to detail.jmte\>** - (default: detail.jmte) Remove this line if you do not require individual \<movie\>.html pages.
* **episode<path to episode.jmte>** - (default: episode.jmte) Only for TV show exporting! This is the template for episode data export.
* **extension={html|xml|csv}** - (default: html) This is the format tMM will export.
* **description=\<text\>** - Write a short description that will print in the tMM exporter UI. Newlines (\n) should be used to insert paragraph breaks.
* **url=\<url to homepage\>** - The URL to the page that hosts this template or to the author's homepage. Remove this line if you have neither.

Using the above information write your template.conf file. It may resemble this example:
```
    name=Jelly is Delicious
    type=movie
    list=list.jmte
    detail=detail.jmte
    extension=html
    description=Created by William Shatner\n\nThis template has jelly in its gears.
    url=https://github.com/TheShatner/jelly_template
```

list.jmte and detail.jmte are HTML pages. The JMTE syntax is used to insert variables like movie name, cast, genre and file information. All of the variables are stored in the list array movies. To access each movies' variables you must itterate over the entire list array.

## Accessing data
In the following code the list array movies is iterated over. For each movie entry we assign the variable movie to hold its details and append the name of a variable to print individual attributes.
```
    <div class="movie details">
    ${foreach movies movie}
        <span class="bold">Title</span>: ${movie.name}
        <span class="bold">Year</span>: ${movie.year}
    ${end}
    </div>
```
As you can see, the name variable in ${movie.name} tells JMTE to print the name of the movie. The variable name is a string, but some movie variables are also list arrays. Print the list array genres with the following code:
```
    ${foreach movies movie}
        ${movie.name}
        <span class="genreList">
        ${foreach movie.genres genre , } // " , " comma is used here as genre seperator
            ${genre}
        ${end}
        </span>
    ${end}
```    
In this example we iterated over the movies list array like in the previous example. Then, from within the first foreach loop, we iterated over the genres list array and printed them. We told JMTE to separate each entry with a comma by putting a comma at the end of the foreach instance.

To access values in a map, you can simply use map key like the variable (to access a key from type enum, you have to use upper case notation)
```
    ${foreach movies movie}
        <img src="${movie.artworkUrls.POSTER(../nopicture.gif)}" />
    ${end}     
```
In this example we exported the url of the first poster with jmte and also provided a fallback `(../nopicture.gif)` if there is no poster.

## Exporting images
You can either embed image urls (see the example above) into your templates or export the images from your library.

**Be aware**: Image urls are not always available. Whenever you scrape a movie/episode in tinyMediaManager we store the image url in our internal database. But if you import existing objects into tinyMediaManager (via NFOs), there are no more urls available and your export template could get incomplete.

We've added a feature to trigger an export of images (with or without scaling) along the meta data export. In the following example you see how a movie poster can be exported via the template:
```
    ${foreach movies movie}
        <img src="../${movie;copyArtwork(type=POSTER, thumb=true, width=180, destination=images, fallback=../nopicture.gif)}" />
    ${end}    
```
To initiate the export process you only need `${movie;copyArtwork(type=...)}`. All other options are optional.

As you see there are some options for exporting available:
* `type=...`: select which artwork type you want to export:
  * POSTER
  * FANART
  * BANNER
  * CLEARART
  * DISCART
  * LOGO
  * CLEARLOGO
  * THUMB
  * CHARACTERART
  * KEYART
  * SEASON_POSTER
  * SEASON_BANNER
  * SEASON_THUMB
* `thumb=...`: rescale the image to a thumbnail? Valid options are `true` and `false`. Default is `false`
* `width=...`: scale to the desired width when creating a thumbnail. Valid is any number. Default: `150`
* `destination=...`: the destination folder where to export the images to. This name is relative to the chosen export folder (e.g. you want to export to `H:\export` and have `img` as destination, all images will be exported to `H:\export\img`). Default: `images`
* `default=../nopicture.gif`: this image will be taken if the desired image can not be exported for this movie
* `escape=...`: set `true` to escape the url to the copied artwork

## Available data
Following variables can be used (***ATTENTION*** - this list is auto generated and may contain fields you will never need):

### Movies ###

|Movies| |
|---|---|
|List\<Person\>|actors|
|Map\<String,MediaFile\>|artworkMap|
|Map\<String,String\>|artworkUrls|
|String|certification|
|String|country|
|String|dataSource|
|Date|dateAdded|
|String|dateAddedAsString|
|UUID|dbId|
|List\<Person\>|directors|
|String|directorsAsString|
|boolean|disc|
|boolean|duplicate|
|String|edition|
|String|editionAsString|
|List\<String\>|extraFanarts|
|List\<String\>|extraThumbs|
|List\<String\>|genres|
|String|genresAsString|
|Boolean|hasImages|
|Boolean|hasMetadata|
|Boolean|hasNfoFile|
|boolean|hasRating|
|Boolean|hasTrailer|
|Map\<String,Object\>|ids|
|String|imdbId|
|Date|lastWatched|
|MediaFile|mainVideoFile|
|List\<MediaFile\>|mediaFiles|
|List\<MediaFile\>|mediaFilesContainingAudioStreams|
|List\<MediaFile\>|mediaFilesContainingSubtitles|
|float|mediaInfoAspectRatio|
|List\<String\>|mediaInfoAudioChannelList|
|String|mediaInfoAudioChannels|
|String|mediaInfoAudioCodec|
|String|mediaInfoAudioCodecAndChannels|
|List\<String\>|mediaInfoAudioCodecList|
|String|mediaInfoAudioLanguage|
|List\<String\>|mediaInfoAudioLanguageList|
|String|mediaInfoContainerFormat|
|double|mediaInfoFrameRate|
|String|mediaInfoSource|
|int|mediaInfoVideoBitDepth|
|int|mediaInfoVideoBitrate|
|String|mediaInfoVideoCodec|
|String|mediaInfoVideoFormat|
|String|mediaInfoVideoResolution|
|String|mediaSource|
|MovieSet|movieSet|
|String|movieSetTitle|
|boolean|multiMovieDir|
|String|note|
|boolean|offline|
|String|originalTitle|
|String|originalTitleSortable|
|String|path|
|String|plot|
|List\<Person\>|producers|
|String|productionCompany|
|Rating|rating|
|Map\<String,Rating\>|ratings|
|Date|releaseDate|
|String|releaseDateAsString|
|String|releaseDateFormatted|
|int|runtime|
|int|runtimeFromMediaFiles|
|int|runtimeFromMediaFilesInMinutes|
|boolean|scraped|
|String|sortTitle|
|String|spokenLanguages|
|boolean|stacked|
|String|tagline|
|List\<String\>|tags|
|String|tagsAsString|
|String|title|
|String|titleForUi|
|String|titleSortable|
|int|tmdbId|
|int|top250|
|List\<MediaTrailer\>|trailer|
|String|video3DFormat|
|String|videoBasenameWithoutStacking|
|List\<MediaFile\>|videoFiles|
|String|videoHDRFormat|
|boolean|videoIn3D|
|boolean|watched|
|List\<Person\>|writers|
|String|writersAsString|
|int|year|

|MovieSet| |
|---|---|
|Map\<String,MediaFile\>|artworkMap|
|Map\<String,String\>|artworkUrls|
|Date|dateAdded|
|String|dateAddedAsString|
|UUID|dbId|
|Map\<String,Object\>|ids|
|List\<MediaFile\>|mediaFiles|
|List\<Movie\>|movies|
|String|originalTitle|
|String|plot|
|String|title|
|String|titleSortable|

### TV Shows ###

|TvShow| |
|---|---|
|List\<Person\>|actors|
|Map\<String,MediaFile\>|artworkMap|
|Map\<String,String\>|artworkUrls|
|String|certification|
|String|country|
|String|dataSource|
|Date|dateAdded|
|String|dateAddedAsString|
|UUID|dbId|
|int|dummyEpisodeCount|
|List\<TvShowEpisode\>|dummyEpisodes|
|boolean|duplicate|
|int|episodeCount|
|List\<TvShowEpisode\>|episodes|
|List\<TvShowEpisode\>|episodesForDisplay|
|List\<MediaFile\>|episodesMediaFiles|
|List\<TvShowEpisode\>|episodesToScrape|
|List\<String\>|extraFanartUrls|
|Date|firstAired|
|String|firstAiredAsString|
|String|firstAiredFormatted|
|List\<String\>|genres|
|String|genresAsString|
|Boolean|hasEpisodeNfoFiles|
|Boolean|hasImages|
|Boolean|hasNfoFile|
|Boolean|hasSeasonAndEpisodeImages|
|Map\<String,Object\>|ids|
|String|imdbId|
|Date|lastWatched|
|MediaFile|mainVideoFile|
|List\<MediaFile\>|mediaFiles|
|float|mediaInfoAspectRatio|
|List\<String\>|mediaInfoAudioChannelList|
|String|mediaInfoAudioChannels|
|String|mediaInfoAudioCodec|
|List\<String\>|mediaInfoAudioCodecList|
|String|mediaInfoAudioLanguage|
|List\<String\>|mediaInfoAudioLanguageList|
|String|mediaInfoContainerFormat|
|double|mediaInfoFrameRate|
|String|mediaInfoSource|
|int|mediaInfoVideoBitDepth|
|String|mediaInfoVideoCodec|
|String|mediaInfoVideoFormat|
|String|mediaInfoVideoResolution|
|String|note|
|String|originalTitle|
|String|path|
|String|plot|
|String|productionCompany|
|Rating|rating|
|Map\<String,Rating\>|ratings|
|int|runtime|
|boolean|scraped|
|int|seasonCount|
|List\<TvShowSeason\>|seasons|
|String|sortTitle|
|String|status|
|List\<String\>|tags|
|String|tagsAsString|
|String|title|
|String|titleSortable|
|List\<MediaTrailer\>|trailer|
|int|traktId|
|String|tvdbId|
|String|videoHDRFormat|
|boolean|videoIn3D|
|boolean|watched|
|int|year|

|TvShowSeason| |
|---|---|
|boolean|dummy|
|List\<TvShowEpisode\>|episodes|
|List\<TvShowEpisode\>|episodesForDisplay|
|Boolean|hasEpisodeImages|
|Boolean|hasEpisodeNfoFiles|
|Boolean|hasImages|
|Date|lastWatched|
|List\<MediaFile\>|mediaFiles|
|int|season|
|TvShow|tvShow|
|boolean|watched|

|TvShowEpisode| |
|---|---|
|List\<Person\>|actors|
|int|airedEpisode|
|int|airedSeason|
|Map\<String,MediaFile\>|artworkMap|
|Map\<String,String\>|artworkUrls|
|String|certification|
|String|dataSource|
|Date|dateAdded|
|String|dateAddedAsString|
|UUID|dbId|
|List\<Person\>|directors|
|String|directorsAsString|
|boolean|disc|
|int|displayEpisode|
|int|displaySeason|
|boolean|dummy|
|int|dvdEpisode|
|boolean|dvdOrder|
|int|dvdSeason|
|int|episode|
|Date|firstAired|
|String|firstAiredAsString|
|String|firstAiredFormatted|
|List\<Person\>|guests|
|Boolean|hasImages|
|Boolean|hasNfoFile|
|Map\<String,Object\>|ids|
|Date|lastWatched|
|MediaFile|mainVideoFile|
|List\<MediaFile\>|mediaFiles|
|List\<MediaFile\>|mediaFilesContainingAudioStreams|
|List\<MediaFile\>|mediaFilesContainingSubtitles|
|float|mediaInfoAspectRatio|
|List\<String\>|mediaInfoAudioChannelList|
|String|mediaInfoAudioChannels|
|String|mediaInfoAudioCodec|
|String|mediaInfoAudioCodecAndChannels|
|List\<String\>|mediaInfoAudioCodecList|
|String|mediaInfoAudioLanguage|
|List\<String\>|mediaInfoAudioLanguageList|
|String|mediaInfoContainerFormat|
|double|mediaInfoFrameRate|
|String|mediaInfoSource|
|int|mediaInfoVideoBitDepth|
|String|mediaInfoVideoCodec|
|String|mediaInfoVideoFormat|
|String|mediaInfoVideoResolution|
|String|mediaSource|
|boolean|multiEpisode|
|String|note|
|String|originalTitle|
|String|parent|
|String|path|
|Path|pathNIO|
|String|plot|
|String|productionCompany|
|Rating|rating|
|Map\<String,Rating\>|ratings|
|int|runtime|
|int|runtimeFromMediaFiles|
|int|runtimeFromMediaFilesInMinutes|
|boolean|scraped|
|int|season|
|boolean|stacked|
|List\<String\>|tags|
|String|tagsAsString|
|String|title|
|String|titleForUi|
|String|titleSortable|
|TvShow|tvShow|
|UUID|tvShowDbId|
|TvShowSeason|tvShowSeason|
|String|tvdbId|
|String|videoBasenameWithoutStacking|
|List\<MediaFile\>|videoFiles|
|String|videoHDRFormat|
|boolean|videoIn3D|
|boolean|watched|
|List\<Person\>|writers|
|String|writersAsString|
|int|year|

### Common Entities ###

|Person| |
|---|---|
|String|name|
|String|nameForStorage|
|String|profileUrl|
|String|role|
|String|thumbUrl|
|String|type|

|Rating| |
|---|---|
|String|id|
|int|maxValue|
|float|rating|
|float|ratingNormalized|
|int|votes|

|MediaFile| |
|---|---|
|boolean|DVDFile|
|boolean|HDR|
|boolean|animatedGraphic|
|float|aspectRatio|
|float|aspectRatioCalculated|
|String|audioChannels|
|List\<String\>|audioChannelsList|
|String|audioCodec|
|List\<String\>|audioCodecList|
|String|audioLanguage|
|List\<String\>|audioLanguagesList|
|List\<MediaFileAudioStream\>|audioStreams|
|String|basename|
|int|bitDepth|
|String|bitDepthString|
|String|biteRateInKbps|
|boolean|blurayFile|
|String|combinedCodecs|
|String|containerFormat|
|MediaFileAudioStream|defaultOrBestAudioStream|
|boolean|discFile|
|int|duration|
|String|durationHHMMSS|
|String|durationHM|
|String|durationHMS|
|int|durationInMinutes|
|String|durationShort|
|String|exactVideoFormat|
|String|extension|
|Path|file|
|Path|fileAsPath|
|long|filedate|
|String|filename|
|String|filenameWithoutStacking|
|long|filesize|
|String|filesizeInMegabytes|
|double|frameRate|
|boolean|graphic|
|boolean|hdDVDFile|
|String|hdrFormat|
|boolean|mainDiscIdentifierFile|
|int|overallBitRate|
|boolean|packed|
|String|path|
|int|stacking|
|String|stackingMarker|
|List\<MediaFileSubtitle\>|subtitles|
|String|subtitlesAsString|
|String|title|
|String|type|
|boolean|video|
|String|video3DFormat|
|String|videoCodec|
|String|videoDefinitionCategory|
|boolean|videoDefinitionHD|
|boolean|videoDefinitionLD|
|boolean|videoDefinitionSD|
|String|videoFormat|
|int|videoHeight|
|String|videoResolution|
|int|videoWidth|

|MediaFileAudioStream| |
|---|---|
|int|audioChannels|
|int|bitrate|
|String|bitrateInKbps|
|String|codec|
|boolean|defaultStream|
|boolean|forced|
|String|language|

|MediaFileSubtitle| |
|---|---|
|String|codec|
|boolean|defaultStream|
|boolean|forced|
|String|language|

|MediaFileType| |
|---|---|
|VIDEO|main video files|
|TRAILER|trailer|
|SAMPLE|sample != trailer|
|AUDIO|audio files|
|SUBTITLE|subtitle files|
|NFO|NFO files|
|POSTER|poster|
|FANART|fanart|
|BANNER|banner|
|CLEARART|clearart|
|DISCART|disc art|
|LOGO|logo|
|CLEARLOGO|clear logo|
|THUMB|thumb|
|CHARACTERART|character art|
|KEYART|key art|
|SEASON_POSTER|season poster|
|SEASON_BANNER|season banner|
|SEASON_THUMB|season thumb|
|EXTRAFANART|extra fanart|
|EXTRATHUMB|extra thumb|
|EXTRA|any extra file|
|GRAPHIC|generic graphic|
|MEDIAINFO|xxx-mediainfo.xml|
|VSMETA|xxx.ext.vsmeta Synology|
|THEME|"theme" files for some skins, like theme.mp3 (or bg video)|
|TEXT|various text infos, like BDinfo.txt or others...|
|UNKNOWN||

|MediaTrailer| |
|---|---|
|String|date|
|String|url|
|Boolean|inNfo|
|String|name|
|String|provider|
|String|quality|
|String|url|

[1]: https://github.com/DJCordhose/jmte
[2]: https://forum.kodi.tv/forumdisplay.php?fid=204
[3]: https://www.reddit.com/r/tinyMediaManager/
[4]: https://gitlab.com/tinyMediaManager/tinyMediaManager/
[5]: /docs/jmte
