---
layout: single
permalink: "/docs/installation"
title: "Installation"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---
tinyMediaManger is a Java application (which needs at least Java 1.7) which will run on Windows as well as Max OSX and Linux. Because there are little differences between these operating systems, tinyMediaManager’s installation behaviour is a bit different.

On every system tinyMediaManager runs an automated update check at startup. This ensures the installation will stay up to date (with the latest bug fixes and features).

<a name="Windows"/>

### Windows
In Windows tinyMediaManager comes in portable format. Inside the (.zip) file there are all the necessairy files from the tinyMediaManager. You should extract all the files from the (.zip) inside a folder, and then either use the tinyMediaManager.exe file, or add a shortcut of it on your desktop.

### Mac OSX
In Mac OSX the application ships as an .app. All files (program files as well as database/cache and logs) are being hold inside the app.

Simply drag and drop the .app from the zip file to your applications folder and start tinyMediaManager from there.

**BE AWARE**: you cannot start tinyMediaManager from within the compressed file (.zip)!

### Linux
In Linux tinyMediaManager comes as a packaged (.zip) file as on <a href="#Windows">Windows</a> Operating System. You should extract all the files from the (.zip) inside a folder, and then either use the tinyMediaManager.sh shell file or you can create a Desktop Shortcut by executing the following script on your terminal **inside from your tinyMediaManager's instance folder**.

```bash
cat <<EOM >~/.local/share/applications/tinyMediaManager.desktop
[Desktop Entry]
Type=Application
Terminal=false
Name=tinyMediaManager
Icon=$PWD/tmm.png
Exec=$PWD/tinyMediaManager.sh
EOM
```

**You should download libmediainfo from [https://mediaarea.net/en/MediaInfo/Download](https://mediaarea.net/en/MediaInfo/Download) if that library is outdated in your distributions package archive!**

Note: In case you get a **blank window** when starting tinyMediaManager, <a href="#wmname-info">see below</a> for a possible solution.

#### Ubuntu/Debian
```
# apt-get install openjdk-8-jdk
# apt-get install libmediainfo
# cd /path/to/tinyMediaManager
# ./tinyMediaManager.sh
```

#### Arch Linux
```
# pacman -S java8-openjdk
# pacman -S libmediainfo
# cd /path/to/tinyMediaManager
# ./tinyMediaManager.sh
```

#### Fedora
```
# dnf install java-1.8.0-openjdk-devel.x86_64
# dnf install libmediainfo
# cd /path/to/tinyMediaManager
# ./tinyMediaManager.sh
```

<a name="wmname-info" />

### Blank window
Users of non-re-parenting Window Managers (e.g. *XMonad*) might get a **blank window** (i.e. a window without anything in it) when starting up tinyMediaManager. This is because the JVM contains a hard-coded list of known non-re-parenting window managers, and certain window managers like XMonad are not included on the list.

Luckily this can be mitigated: install the **wmname** utility via your package manager, or from [https://tools.suckless.org/x/wmname](https://tools.suckless.org/x/wmname). Then, *before* you run the command `tinyMediaManager.sh`, run the command: `wmname LG3D`. This will instruct the shell to impersonate LG3D, the non-re-parenting window manager written in Java by Sun. Some ugly background info [here](https://web.archive.org/web/20161022025923/https://awesome.naquadah.org/wiki/Problems_with_Java).

You will have to do this each time before starting up tinyMediaManager, so it's probably easiest to add this into a shell script. For instance, add the following to a new file `tmm.sh` inside your tinyMediaManager directory:

```bash
#!/bin/bash
wmname LG3D
exec $(dirname)/tinyMediaManager.sh
```

and run `chmod +x tmm.sh` once afterwards.

To start tinyMediaManager, run this script (`./tmm.sh`) instead of `tinyMediaManager.sh`.

<a name="contentlocation" />

### Data/Settings/Cache/Backup locations
tinyMediaManager is designed to encapsulate all generated data as settings, databases, cache, logs and backups into its own folder. But there are some setups which required to have these data in another folder. The following system environment variables help to get these data written to another folders:

```
-Dtmm.contentfolder
```

set the parent folder of all these _data_ folders. e.g. `-Dtmm.contentfolder=/home/user/.tmm`

```
-Dtmm.datafolder
-Dtmm.cachefolder
-Dtmm.logfolder
-Dtmm.backupfolder
```

can set all folders differently. e.g. `-Dtmm.datafolder=/home/user/.tmm/data`, `-Dtmm.logfolder=/var/log/tmm`.

These parameters can either be set via an file called `extra.txt` inside the tinyMediaManager folder (statically) or via the starting shellscript (dynamically - with variable expansion like `-Dtmm.contentfolder=${HOME}/.tmm`).
