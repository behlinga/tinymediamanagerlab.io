---
layout: single
permalink: "/docs/commandline"
title: "Command Line Arguments"

sidebar:
  nav: "docs"
---
To start tinyMediaManager via the command line you can use the dedicated executeable/shellscript for command line arguments:

* Windows: **tinyMediaManagerCMD.exe**
* Linux: **tinyMediaManagerCMD.sh**
* macOS: **tinyMediaManagerCMD-OSX.sh** (note: the shellscript is inside the tinyMediaManager.app, in the subfolder `Resources\Java`)

Execute without any parameter to display the syntax.

| Parameter | |
| --------- | --- |
| **-updateMovies** | update all movie datasources and add new movies/files to the database |
| **-updateMoviesX** | replace X with 1-9 - just updates a single movie datasource; ordering like GUI |
| **-updateTv** | update all TvShow datasources and add new TvShows/episodes to the database |
| **-updateTvX**| replace X with 1-9 - just updates a single TvShow datasource; ordering like GUI |
| **-update** | update all (short for ‘-updateMovies -updateTv’) |
| **-scrapeNew** | auto-scrape (force best match) new found movies/TvShows/episodes from former update(s) |
| **-scrapeUnscraped** | scrape all unscraped movies (independent from update) |
| **-scrapeAll** | ALL movies/TvShows/episodes, whether they have already been scraped or not |
| **-rename** | rename & cleanup all the movies/TvShows/episodes from former scrape command |
| **-config \<file.xml\>** | specify an alternative configuration xml file in the data folder |
| **-export \<template\> \<dir\>** | exports your complete movie/tv library with specified template to dir |
| **-checkFiles** | does a physical check, if all files in DB are existent on filesystem (might take long!) |

so a ```tinyMediaManagerCMD.exe -update -scrapeNew -renameNew``` perfectly cleans your new and updated items :)

NOTE: if you start tinyMediaManager this way, you won’t get an UI or updates!
You can update tinyMediaManager either by starting the UI or starting the updater executable for your system:

* Windows: **tinyMediaManagerCMDUpd.exe**
* Linux: **tinyMediaManagerUpdaterCMD.sh**
* macOS: **tinyMediaManagerUpdaterCMD-OSX.sh** (note: the shellscript is inside the tinyMediaManager.app, in the subfolder `Resources\Java`)
