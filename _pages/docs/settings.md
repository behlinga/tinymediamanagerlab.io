---
layout: single
permalink: "/docs/settings"
title: "Settings"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---
You can access the settings via the menu "tinyMediaManager - Settings" on top of the tinyMediaManager window.

tinyMediaManager stores its settings in some files (`tmm.json`, `movies.json`, `tvshows.json`, ..) inside the folder `data` of your tinyMediaManager installation.

The settings are divided into several sections

* The section General Settings contains all "module independent" settings for tinyMediaManager like UI settings (language, font, theme, ..), recognized file types (video, audio, additional, ..) and system settings.
* [Movie Settings][1] contains all movie related settings for tinyMediaManager.
* [TV Show Settings][2] contains all TV show related settings for tinyMediaManager.

## General
In this section you can change some settings for the UI, system and connectivity:

### UI language ###
Select your preferred language for the UI. tinyMediaManager is translated to several different languages, if you feel that your desired language is missing or incomplete, please join us at [Weblate][3] to translate tinyMediaManager.

### UI Theme ###
tinyMediaManger offers a _light_ and a _dark_ theme. After changing the theme a restart is needed.

### Font ###
This section offers settings for the UI font: you can choose the desired *font* and *font size* for tinyMediaManager.

Please be aware: tinyMediaManager is designed to use the font Dialog with a font size of 12. If you change the font/size there might be some layout issues.

### Misc. settings ###
This section covers some other settings like _storing the window sizes/locations_ or _show the memory usage_

## File types ##

* **Video file types**
* **Subtitle and /additional file types**
* **Audio file types**

In these sections you can specify which file types should be analysed when scanning for content.

### Unwanted file types ###

In this section you can enter file types to be found by the _cleanup unwanted files_ action. You can also use regular expressions to find unwanted files. Below are some of the Commands you can combine

| 1. | Common matching symbols |
|-----------|--
|`.`        | Matches any character
|`^regex`   | Finds regex that must match at the beginning of the line.
|`regex$`   | Finds regex that must match at the end of the line.
|`[abc]`    | Set definition, can match the letter a or b or c.
|`[abc][vz]`| Set definition, can match a or b or c followed by either v or z.
|`[^abc]`   | When a caret appears as the first character inside square brackets, it negates the pattern.
|`[a-d1-7]` | Ranges: matches a letter between a and d and figures from 1 to 7, but not d1.
|`X\|Z`     | Finds X or Z.
|`XZ`       | Finds X directly followed by Z.
|`$`        | Checks if a line end follows.

The following meta characters have a pre-defined meaning and make certain common patterns easier to use. For example, you can use \d as simplified definition for [0..9].

| 2. | Meta characters |
|--|--|
| `\d`      | Any digit, short for [0-9]|
| `\D`      | A non-digit, short for [^0-9]|
| `\s`      | A whitespace character, short for [ \t\n\x0b\r\f]|
| `\S`      | A non-whitespace character, short for|
| `\w`      | A word character, short for [a-zA-Z_0-9]|
| `\W`      | A non-word character [^\w]|
| `\S+`     | Several non-whitespace characters|
| `\b`      | Matches a word boundary where a word character is [a-zA-Z0-9_]|

A quantifier defines how often an element can occur. The symbols ?, \*, + and {} are qualifiers.

| 3. | Quantifiers | |
|--|--|--|
|`*`   |Occurs zero or more times, is short for {0,} | X* finds no or several letter X, .* finds any character sequence |
|`+`   |Occurs one or more times, is short for {1,} | X+- Finds one or several letter X |
|`?`   |Occurs no or one times, ? is short for {0,1}. | X? finds no or exactly one letter X |
|`{X}` |Occurs X number of times, {} describes the order of the preceding liberal | \d{3} searches for three digits, .{10} for any character sequence of length 10. |
|`{X,Y}`|Occurs between X and Y times, | \d{1,4} means \d must occur at least once and at a maximum of four. |
|`*?`  |? after a quantifier makes it a reluctant quantifier. It tries to find the smallest match. This makes the regular expression stop at the first match. |  |

## Title sorting ##

This Area contains a list of words/prefixes, which will moved from the beginning to the end of the movie, TV show and episode titles. For example:  

**The Bourne Identity** will become **Bourne Identity, The**  
This will now alphabetically sort in within "B".

If you do not want this feature to happen, just delete all entries from that list in settings...

## External devices ##

### Wake-on-LAN ###

You can enter a list of Wake-on-LAN devices which will be available for the action: `Tools > Wake-on-LAN`

### Kodi/XBMC ###

You can connect to a Kodi/XBMC device for remote function calls (experimental).

### UPnP ###

You can use tinyMediaManger as a UPnP server or remote play content on remote UPnP clients.

## External Services ##

### Trakt.tv ###

tinyMediaManger has a complete integration to the services Trakt.tv to synchronize your media library and watched states.

Trakt.tv uses a PIN auth to authorize external clients to connect to their service. First of all you need to create a Trakt.tv account. After that you can connect tinyMediaManager to your Trakt.tv account with pressing the button "Get PIN code for trakt.tc access". You will be redirected to the Trakt.tv website where you get your PIN. Enter this PIN in tinyMediaManager and you will be connected.

By clicking the button "Test connection to trakt.tv" you can test the connection to Trakt.tv

## System ##

### Media Player ###

Per default tinyMediaManager invokes the system standard media player to launch your video files. With this setting you can specify a media player which will be used for playing the files.

### Memory settings ###

In this section you can specify the amount of memory which tinyMediaManager can use. Per default we use max 512MB - but with larger media libraries you may have to increase this setting.

**BE AWARE**: Java needs this amount of memory to be available (free and in one chunk!) on startup of tinyMediaManager. If there is not enough memory available on your system, Java prevents tinyMediaManager from starting _without_ any message. If you encounter this problem, just try to lower the memory setting directly in the file `extra.txt` in your tinyMediaManger folder.

### Proxy Settings ###

Here you can enter proxy settings, if you are using a proxy to connect to the internet. **Attention**: NTLM proxies do not work at the moment and you have to use something like [px-proxy][] or [CNTLM][2] to connect to the internet.

### Misc. settings ###

* **Disable SSL certificate verification**: sometimes something goes wrong with SSL certificates. If you encounter a unrecoverable problem with SSL certificates, you can disable the SSL certificate check here.


## Misc. settings ##

* **Delete trash/backup files folder on exit**: tinyMediaManager is using a trash/backup folder on every data source (.deletedByTMM) where deleted files and folders will be moved to. If you activate this setting, this folder will be deleted on shutdown of tinyMediaManager
* **Enable image cache**: Image files which resist in your movie/TV show folders are sometimes huge and/or not accessible all the time (e.g. powered off NAS). With this option checked all loaded images will be cached locally (in the install folder), with a slightly lower resolution and quality. There is also a function, which will cache all images of all entries in your database (Debug -> Cache -> rebuild image cache). **Attention**: this is an "expensive" operation – it will take some time with maximum CPU load. After you have built the cache, all images will be shown in tinyMediaManager, even if the datasource is not reachable.
  * **Image cache type**: _SMOOTH_ or _FAST_ - this describes the algorithm of scaling/re encoding the cached images. SMOOTH costs little more CPU time, but creates better images, whereas FAST cost less CPU time, but the images aren't that good.

[1]: /docs/movies/settings
[2]: /docs/tvshows/settings
[3]: https://hosted.weblate.org/projects/tinymediamanager/translations/
[4]: https://github.com/genotrance/px
[5]: http://cntlm.sourceforge.net/
