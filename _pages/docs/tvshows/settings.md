---
layout: single
permalink: "/docs/tvshows/settings"
title: "TV Show Settings"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

# TV Show Settings #

## TV Shows ##

### UI Settings ###

* **Persist UI filters**: When active, the filter set in the TV show list will be stored and loaded on tinyMediaManager start up.
* **Show media logos**: Show/hide the media logos panel in the TV show details view
* **Display missing episodes**: tinyMediaManger is able to mix in missing episode if the TV show has been scraped previously (to know which episodes are available). When activating this option, the missing episodes will be mixed into the TV show list.
  * **Display missing specials**: Also display missing specials (episodes from season 0)
  * **Preferred rating**: tinyMediaManager supportes multiple ratings for every TV show/episode. The rating source in this setting indicates which rating source should be preferred for being used in the UI.
  * **Personal rating first**: when active any personal rating will be preferred over all other ratings

### Automatic Tasks ###

* **Automatic rename**: you can let tinyMediaManager automatically rename your TV shows/episodes after they have been scraped
* **Automatic sync with Trakt.tv**: if you have enabled access to Trakt.tv, you are able to automatically sync your media library and watched state with Trakt.tv. Is that option is disabled, you have to manually trigger the synchronization process

### Misc. Settings ###

* **Build image cache on import**: when importing new TV shows/episodes into tinyMediaManager you can create the image cache on the fly (only available when the image cache is enabled in the general settings). **Caution**: building the image cache will take a while
* **The following images types will be checked for artwork completeness**: tinyMediaManager needs to know which artwork types must be available to know if the TV show/episode artwork is complete. This is needed for the artwork column in the TV show list and the action _Download missing artwork_.

### Media Center Presets ###

tinyMediaManager has presets for various media centers. By pressing the button for your desired media center, all affected settings will be set to the default for the chosen media center.

Currently we support presets for:

* Kodi (17+)
* XBMC/Kodi (<17)
* MediaPortal 1.x (Moving Pictures and MyVideo)
* MediaPortal 2.x
* Plex

## Data Sources ##

### Data Sources ###

This is the core setting of the TV show section.  Every folder specified in this list will be searched for movie files. While the TV show importer tries to parse out as much as possible (seasons, episodes, names..) you have to follow one strict rule: the folders inside the data source have to be the root folders for your tv shows – each folder in your data source contains **exactly one** TV show. Further nesting is allowed and should be no problem for the importer, as well as different file name notations:

```
H:\tv_shows
    Breaking Bad
        bb_S01E01.avi
        S01E02.mkv
        ...
        Season 2
            S02E01.avi
            Breaking.Bad.S02E02-Grilled.mkv
            ...
    Castle
        Castle-S01E01.avi
        S01E02.avi
        S02E01.mkv
        S02E02.avi
    ...
```

Since we have a large rule set of regular expressions to detect episode/season number, there are still some notations which are problematic. The best results are always with a filename containing season and episode information with the following scheme SxxExx, along with a directory structure like:

```
<TV show folder>
    Season 1
        <title>-S01E01.avi
        <title>-S01E02.avi
        ...
    Season 2
        <title>-S02E01.avi
        <title>-S02E02.avi
        ...
    ...
```

tinyMediaManager (and Java) supports accessing local drives and network shares, but is unable to perform mount/logon actions. You have to connect to external drives from your system **before** starting an _update data sources_ in tinyMediaManager!

* **Imported episodes are in DVD order**: When importing a new episode, tinyMediaManager has to decide whether the detected season/episode number is in aired order or DVD order. With this setting you can switch the default.

### Exclude Folder(s) from Scan ##

Every folder in this list will be excluded from a scan. You can also put a file called `.tmmignore` into every folder you wish to have excluded from the scan.

### Bad Words ###

Words from this list will be removed from file names while parsing for the title. This comes in handy if you some special words like release groups in your file names.

## Scraper ##

In this list you can choose your preferred metadata scraper from a list of all available scrapers. At the moment tinyMediaManager offers scrapers for the following meta data sources:

* thetvdb.com
* themoviedb.org
* imdb.com
* trakt.tv

You will find a detailed description of those scrapers on the [scraper description page](/docs/scrapers).

## Scraper Options ##

### Advanced Options ###

* **Preferred language**: Choose the preferred language for scraping (for localized content like title, tagline and plot). Not all scrapers offer localized content, but tinyMediaManager tries to find localized in this language.
* **Certification country**: Movie certifications are available for several countries. You can choose for which country tinyMediaManager should try to get the certification.
* **Capitalize first letter of every word in title and original title**: As the option itself tells - when activating this, tinyMediaManager puts the found title/original title to *Title Case*.

### Metadata Scrape Defaults ###

In this section you can set which types of metadata should be scraped per default. You can always override this in the scrape dialogs in tinyMediaManager.

The command line version of tinyMediaManager completely relies on this setting.

### Images ###

* **Automatically scrape images**: with this option enabled, tinyMediaManager tries to find the best image files from the artwork scrapers according to your settings in the _Images_ section.

## NFO Settings ##

* **NFO format**: There is support for Kodi and MediaPortal NFO formats. Choosing the right format affects how the data is written to the NFO files.
* **NFO file naming**: You can choose between different file names for the NFO files. If no NFO file name is selected, tinyMediaManager does not write any NFO files.
  * TV shows:
    * tvshow.nfo
  * Episodes:
    * \<episode filename\>.nfo
* **NFO language**: The language in which texts like genre names should be written to the NFO file
* **Certification format**: The format of the data in the \<certification\> tag.
* **Write clean NFO**: If this option is activated, tinyMediaManager write a _clean_ NFO file without embedding _unknown/unsupported_ data from existing NFO files.

## Images ##

### Artwork Scraper ###

In this list you can choose the artwork scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible artwork. At the moment tinyMediaManager offers scrapers for the following artwork sources:

* thetvdb.com
* themoviedb.org
* fanart.tv

### Advanced Options ###

* **Preferred language**: Get the artwork with text in the given language. `-` indicates that tinyMediaManager should look for artwork without any texts on it. Please note that not all artwork sources offer the needed information for this to work (it is known that themoviedb.org and fanart.tv work with this setting).
* **Poster size**: Choose the preferred poster size. tinyMediaManager will try to get the poster in this size (and the chosen language). If it’s not available an image from the other sizes will be taken.
* **Fanart size**: Choose the preferred fanart size. tinyMediaManager will try to get the fanart in this size (and the chosen language). If it’s not available an image from the other sizes will be taken.
* **Prefer language over resolution when choosing images**: If the desired artwork size has not been found in the desired language, take a lower size image with the preferred language rather than a bigger artwork in another language

## Artwork Filenames ##

Available artwork settings for TV shows:

* **Poster**
  * poster.ext
  * movie.ext
* **Fanart**
  * fanart.ext
* **Banner**
  * banner.ext
* **Clearart**
  * clearart.ext
* **Thumb**
  * thumb.ext
  * landscape.ext
* **Logo**
  * logo.ext
* **Clearlogo**
  * clearlogo.ext
* **Disc art**
  * disc.ext
  * discart.ext
* **Characterart**
  * characterart.ext
* **Keyart**
  * keyart.ext

Available artwork settings for seasons:  

* **Season poster**
  * seasonXX-poster.ext
  * \<season folder\>/seasonXX.ext
  * \<season folder\>/folder.ext
* **Season banner**
  * seasonXX-banner.ext
  * \<season folder\>/seasonXX-banner.ext
* **Season thumb**
  * seasonXX-thumb
  * \<season folder\>/seasonXX-thumb.ext

`XX` stands for the season number.

Available artwork settings for episodes:  

* **Episode thumb**
  * \<episode filename\>-thumb.ext
  * \<episode filename\>.ext
  * \<episode filename\>.tbn

`.ext` stands for the image format of the artwork file which should be either `.png`, `.jpg` or `.gif`.

## Trailer ##

In this list you can enable all wanted trailer scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible trailers. At the moment tinyMediaManager offers scrapers for the following trailer sources:

* themoviedb.org

### Advanced Options ###

* **Use preferred trailer settings**: Rather than choosing the first available trailer, you can set your preferred trailer source (e.g. Youtube) and trailer quality (e.g. 1080p).
* **Automatic trailer download**: Upon scraping, also download the chosen trailer to your movie folder if no local trailer has been found
* **Trailer file naming**: You can set the desired trailer filename. If no desired filename is set, no trailer will be downloaded.
  * tvshow-trailer.ext

## Subtitles ##

In this list you can enable all wanted subtitle scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible subtitles. At the moment tinyMediaManager offers scrapers for the following subtitle sources:

* opensubtitles.org

### Advanced Options ###

* **Preferred language**: The preferred language for subtitle download.
* **Subtitle language style**: Save subtitle files with the given language style in their filename.

## Renamer ##

tinyMediaManager offers a powerful renamer to rename your TV shows and all associated files to your desired folder-/filenames. While there is almost nothing you can't do with the renamer, it has still one big restriction: you can only rename the TV shows inside its own data source. Renaming it to a destination which is not inside the own data source is not supported.

**Folder name**, **Season folder name** and **Episode filename**: Choose the desired folder name(s) and filename for renaming. Here you can enter fixed parts of the name and dynamic ones. You will find a list of all available tokens for building up dynamic names beneath the settings along with examples of your media library. With leaving the folder name/filename empty, the renamer will skip the name generation for the empty part.

### Advanced Options ###

* **Use the name "Special" for season 0**: Instead of creating season 0 use the term special for creating the file names.
* **Replace spaces in filename**: You can replace all spaces with either underscores, dots or dashes by activation this option.
* **Replace colons with**: Since colons are illegal characters in folder names and filenames, tinyMediaManger offers you to choose how they should be replaced.
* **Replace non ASCII characters with their basic ASCII equivalents**: Some file systems might need to have ASCII conform file names - by activating this option tinyMediaManager tries to convert non ASCII characters into a similar ASCII character (e.g. Ä - Ae; ß - ss, ...).

The renamer is built on [JMTE](https://gitlab.com/tinyMediaManager/tinyMediaManager/wikis/JMTE) and can profit from all options you can have in JMTE. For easier usage, we have added some preset tokens (see table below). Nevertheless you can also access every field from the movie just by using the full syntax like `${tvShow.title}` or `${episode.title}`.

Available tokens:

| Token | Description | Example |
|-------|-------------|---------|
|${title}<br>${episode.title}|Episode title |	Pilot|
|${originalTitle}<br>${episode.originalTitle} |	Episode original title|Pilot|
|${titleSortable}<br>${episode.titleSortable} |	Episode title sorting (e.g. Luminous Fish Effect, The)|	Pilot|
|${seasonNr}<br>${episode.season;number(%d)} | Season number	|1|
|${seasonNr2}<br>${episode.season;number(%02d)}	| Season number with 2 digits	|01|
|${seasonNrDvd}<br>${episode.dvdSeason;number(%d)}	| DVD season number	|1|
|${seasonNrDvd2}<br>${episode.dvdSeason;number(%02d)}	| 2 digit DVD season number|	01|
|${episodeNr}<br>${episode.episode}	| Episode number	|1|
|${episodeNr2}<br>${episode.episode;number(%02d)}	| Episode number with 2 digits|	01|
|${episodeNrDvd}<br>${episode.dvdEpisode}	| DVD episode number	|1|
|${episodeNrDvd2}<br>${episode.dvdEpisode;number(%02d)} | 2 digit DVD episode number|	01|
|${airedDate}<br>${episode.firstAired;date(yyyy-MM-dd)} | Episode aired date (yyyy-MM-dd)	|2008-01-20|
|${year}<br>${episode.year} |	Episode Year	|2008|
|${seasonName}<br>${season.title} |	Season Name	|My personal note|
|${showTitle}<br>${tvShow.title} |	TV show title	|Breaking Bad|
|${showYear}<br>${tvShow.year} |	TV show Year	|2008|
|${showOriginalTitle}<br>${tvShow.originalTitle} |	TV show original title|	Breaking Bad|
|${showTitleSortable}<br>${tvShow.titleSortable} |	TV show title sorting (e.g. 4400, The)|	Breaking Bad|
|${showNote}<br>${tvShow.note} | TV show note | My personal note |
|${videoResolution}<br>${episode.mediaInfoVideoResolution} |	Video resolution (e.g. 1280x720)|	1280x720|
|${videoFormat}<br>${episode.mediaInfoVideoFormat} |	Video format (e.g. 720p)	|720p|
|${videoCodec}<br>${episode.mediaInfoVideoCodec} |	Video codec (e.g. h264)	|h264|
|${videoBitDepth}<br>${episode.mediaInfoVideoBitDepth} |	Video bit depth (e.g. 8 / 10)	|8|
|${audioCodec}<br>${episode.mediaInfoAudioCodec} |	Audio codec of the first stream (e.g. AC3)	|AAC|
|${audioCodecList}<br>${episode.mediaInfoAudioCodecList} |	Array of all audio codecs (e.g. [AC3, MP3])	|[AAC, MP3]|
|${audioCodecsAsString}<br>${episode.mediaInfoAudioCodecList;array} |	List of all audio codecs (e.g. AC3, MP3)	|AAC, MP3|
|${audioChannels}<br>${episode.mediaInfoAudioChannels} |	Audio channels of the first stream (e.g. 6ch)	|6ch|
|${audioChannelList}<br>${episode.mediaInfoAudioChannelList} |	Array of all audio channels (e.g. [6ch, 2ch])|	[6ch, 2ch]|
|${audioChannelsAsString}<br>${episode.mediaInfoAudioChannelList;array} |	List of all audio channels (e.g. 6ch, 2ch)	|6ch, 2ch|
|${audioLanguage}<br>${episode.mediaInfoAudioLanguage} |	Audio language of the first stream (e.g. EN)|	en|
|${audioLanguageList}<br>${episode.mediaInfoAudioLanguageList} |	Array of all audio languages (e.g. [EN, DE])|	[en, de]|
|${audioLanguagesAsString}<br>${episode.mediaInfoAudioLanguageList;array} |	List of all audio languages (e.g. EN, DE)|	en, de|
|${mediaSource}<br>${episode.mediaSource} |	Media source (DVD etc)|	TV|
|${hdr}<br>${episode.videoHDRFormat} |	High Dynamic Range (HDR)|	HDR|
|${filesize}<br>${episode.videoFilesize;filesize} | File size in GB | 1.05 G |
|${parent}<br>${tvShow.parent} |	Path between datasource and parent folder of the TV show|	B|
|${note}<br>${episode.note} | Episode note | My personal note |

* you can also use some pre-built renderers to modify the result; you cannot use the preset token for the renderers to work - you need to use the full syntax:
  * _UPPER_ case renderer `upper`: `${tvShow.title;upper}`
  * _lower_ case renderer `lower`: `${tvShow.title;lower}`
  * _Title_ case renderer `title`: `${tvShow.title;title}`
  * Date format renderer `date`: `${episode.firstAired;date(yyyy-MM-dd)}` would cause the aired date to be formatted with a renderer named "date". This named renderer would then be passed the format "yyyy-MM-dd" along with the variable to be rendered.

  ### Example ###

  Here you can see the renamer results using an example from your library.
