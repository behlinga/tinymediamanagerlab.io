---
layout: splash
permalink: "/features/"
title: "Features"

---
<!-- global features -->
<div id="features">
  <h1 class="feature__title">Features</h1>
  <div class="feature_wrapper">
    <div class="feature_item">  
      <h3 class="archive__item-title">Multi OS support</h3>
        tinyMediaManager is designed to run on Windows, Linux and Mac OSX.
    </div>

    <div class="feature_item">  
      <h3 class="archive__item-title">Automatic updates</h3>
        tinyMediaManager has an integrated updating system.
    </div>

    <div class="feature_item">  
      <h3 class="archive__item-title">Command line support</h3>
        tinyMediaManager supports command line arguments. With this function you are able to call tinyMediaManager functions from other tools.
    </div>
  </div>
</div>

<!-- movie features -->
<div id="movies" class="full-width-background">
  <h1 class="feature__title">Movie Management</h1>
  <div class="feature_wrapper">
    <div class="feature_item">  
      <h3 class="archive__item-title">Scrape meta data</h3>
        tinyMediaManger gets all necessary meta data for your movies from <b><a href="https://www.themoviedb.org" target="_blank">TheMovieDB.org</a></b>, <b><a href="http://www.imdb.com" target="_blank">Imdb.com</a></b>, <b><a href="http://www.ofdb.de/" target="_blank">OFDb.de</a></b>, <b><a href="http://www.moviemeter.nl" target="_blank">Moviemeter.nl</a></b> and even more.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Get artwork</h3>
      Artwork such as fanart, poster, clearart, discart, logos and more can be downloaded from <b><a href="https://www.themoviedb.org" target="_blank">TheMovieDB.org</a></b> and <b><a href="https://fanart.tv" target="_blank">Fanart.tv</a></b>.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Get trailers</h3>
      Get a list of available trailers for your movies from <b><a href="https://www.themoviedb.org" target="_blank">TheMovieDB.org</a></b> and <b><a href="http://www.hd-trailers.net" target="_blank">HD-Trailers.net</a></b>.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Get subtitles</h3>
      Get a list of available subtitles for your movies from <b><a href="http://www.opensubtitles.org" target="_blank">OpenSubtitles.org</a></b>.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Edit meta data</h3>
      If you aren't satisfied with the scraper results, you can manually change all meta data/artwork/trailer.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Rename movie files</h3>
      tinyMediaManager supports you to organize your file structure. You can rename your movie files and folders files to suit your needs.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Powerful searching</h3>
      In tinyMediaManager you can search, sort and filter movies by many criteria. This allows you a fast access to the movies you are searching for.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Create NFOs</h3>
      tinyMediaManager will produce Kodi and MediaPortal compatible NFOs as well as import NFOs written by other tools (like EmberMediaManager). This will allow an easy migration to tinyMediaManager.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Extract media information</h3>
      The mediainfo library is used to extract technical metadata from your movie files (i.e. video codec, resolution, bitrate, audio channels, ..).
    </div>

  </div>
</div>

<!-- movie set features -->
<div id="moviesets">
  <h1 class="feature__title">Movie Set Management</h1>
  <div class="feature_wrapper">

    <div class="feature_item">
      <h3 class="archive__item-title">Create movie sets</h3>
        Movie sets represents movies which have some sort of connection to each other. Simply think about the Harry Potter Collection: it includes all Harry Potter movies. tinyMediaManager helps you to organize your movies into movie sets.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Artwork for movie sets</h3>
        There is also artwork for movie sets: you can take artwork from <b><a href="https://www.themoviedb.org" target="_blank">TheMovieDB.org</a></b> or local files and store them for use with Kodi/MediaPortal.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Assign movies</h3>
        Since most of the movie sets are already maintained in <b><a href="https://www.themoviedb.org" target="_blank">TheMovieDB.org</a></b>, tinyMediaManager can use this information to automatically assign movies into the right movie sets.
    </div>

  </div>  
</div>

<!-- TV show features -->
<div id="tvshows" class="full-width-background">
  <h1  class="feature__title">TV Show Management</h1>
  <div class="feature_wrapper">

    <div class="feature_item">
      <h3 class="archive__item-title">Powerful import engine</h3>
      tinyMediaManager has a powerful engine to import the file structure of your TV shows. It has many regular expressions to detect the episode and season information out of the file/directory names.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Scrape meta data</h3>
      tinyMediaManger gets all necessary meta data for your TV shows from <b><a href="http://thetvdb.com" target="_blank">TheTVDB.com</a></b>, <b><a href="https://www.themoviedb.org" target="_blank">TheMovieDB.org</a></b> and <b><a href="http://www.imdb.com" target="_blank">Imdb.com</a></b>.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Get Artwork</h3>
      Download artwork for your TV shows and episodes from <b><a href="http://thetvdb.com" target="_blank">TheTVDB.com</a></b>, <b><a href="https://www.themoviedb.org" target="_blank">TheMovieDB.org</a></b> and <b><a href="https://fanart.tv" target="_blank">Fanart.tv</a></b>.
    </div>

   <div class="feature_item">
     <h3 class="archive__item-title">Get subtitles</h3>
     Get a list of available subtitles for your TV shows from <b><a href="http://www.opensubtitles.org" target="_blank">OpenSubtitles.org</a></b>.
   </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Edit meta data</h3>
      If you aren't satisfied with the scraper results, you can manually change all meta data and artwork.
    </div>

    <div class="feature_item">
      <h3 class="archive__item-title">Create NFOs</h3>
      tinyMediaManager will produce Kodi compatible NFOs as well as import NFOs written by other tools (like EmberMediaManager). This will allow an easy tool migration to tinyMediaManager.
    </div>

  </div>
</div>
