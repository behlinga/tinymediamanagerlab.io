---
title: Version 2.9.10
permalink: /blog/version-2-9-10/
categories:
  - News
  - Release
tags:
  - v2  
summary:
  - image: release.png
---
\+ feature: Kodi plugins now able to download images (movies)  
x fixed registering donator version  
