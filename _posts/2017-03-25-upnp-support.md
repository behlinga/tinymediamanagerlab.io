---
title: UPnP support
permalink: /blog/upnp-support/
categories:
  - News
  - Feature

---
With version 2.9.3 we've integrated a very special feature - tinyMediaManager is now able to be a UPnP streaming server.

Just an information as starter: It is only an experimental feature; and only a few UPnP functions have been implemented. There's even a small possibility that this won't work at all with your devices! Although UPnP is a standard, not all vendors stick to it.

To try it for yourself, just activate the UPnP settings, and restart tinyMediaManager!

## Share library

This feature exposes your library to all UPnP aware devices in your LAN. You can browse your complete tinyMediaManager library, and play all videos. Either from Kodi, a tablet, your smartphone, a Smart-TV... anything which has an UPnP feature, and understands this type/codec/format of video, should be usable.

Note: there's no security in place - everybody in your (w)LAN will be able to play your complete library.
  <a class="fancybox" href="{{ site.urlimg }}2017/03/upnp_discovery.png" rel="post" title="UPnP discovery">![Kodi scraper]({{ site.urlimg }}2017/03/upnp_discovery.png "UPnP discovery")</a>

## Remote play

This is the opposite way. We try to identify all UPnP devices in your (w)LAN, which told us, they are able to play video files. So, if you click on our "play" button (and tinyMediaManager found some UPnP devices) you should see now another drop down to choose the playback device.
<a class="fancybox" href="{{ site.urlimg }}2017/03/upnp_remote_play.png" rel="post" title="UPnP remote play">![Kodi scraper]({{ site.urlimg }}2017/03/upnp_remote_play.png "UPnP remote play")</a>
