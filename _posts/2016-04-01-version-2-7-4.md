---
title: Version 2.7.4
permalink: /blog/version-2-7-4/
categories:
  - News
  - Release
tags:
  - v2    
summary:
  - image: release.png
---
\+ added vote count for Tv shows and episodes  
x fixed TvShow renamer when having duplicate tokens or multi episodes  
x fixed MovieRenamerPreview when using a sorted list  
x fixed TMM restart (when resetting database)  
x Display information, why plugins could not be loaded.  
x fixed copy/paste hotkeys for several UI field on OSX  
x fixed exporter file name creation  
x fixed writing set info for MediaPortal NFO files  
x fixed movie import with folder stacking  
