---
title: Evolution of tinyMediaManager (08/2012 - 01/2019)
permalink: /blog/evolution-of-tinymediamanager/
categories:
  - News
tags:
  - v2
  - v3
summary:
  - image: tmm.png
---
Evolution of the tinyMediaManager source code from 08/2012 - 01/2019 rendered via [gource](https://github.com/acaudwell/Gource/)

{% include video id="Q2lLM5OZIj4" provider="youtube" %}
